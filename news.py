import os
import bs4
import requests
def collect():
    # Файл куда скрипт пишет новости
    f = open('новости.txt','w')
    
    # Массив всех новостей
    allNews = []

    # URL с которого получаем новости
    url = "https://www.opennet.ru/opennews/"
    
    # Получаем HTML разметку
    request = requests.get(url)

    soup = bs4.BeautifulSoup(request.text, "html.parser")
    
    # Поиск новостей
    allNews = soup.findAll("a",class_="title2")
    
    # Запись в файл
    for data in allNews:
        f.write(data.text + '\n')
collect()
